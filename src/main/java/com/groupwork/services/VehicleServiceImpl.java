package com.groupwork.services;

import com.groupwork.entities.Vehicle;
import com.groupwork.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService{
    @Autowired
    private VehicleRepository vehicleRepository;


    @Override
    public List<Vehicle> getVehicles() {

        List<Vehicle> vehicles = new ArrayList<>();

        vehicles = vehicleRepository.findAll();
        return vehicles;
    }


    @Override
    public Optional<Vehicle> getById(Long id) {

        return vehicleRepository.findById(id);
    }


    @Override
    public List<Vehicle> getByYear(Integer year) {

        List<Vehicle> vehicles = null;
        vehicles = vehicleRepository.findByYear(year);
        return vehicles;
    }


    @Override
    public Vehicle getByMakeAndModel(String make, String model) {
        Vehicle vehicles = null;
        vehicles = vehicleRepository.findByMakeAndModel(make, model);
        return vehicles;
    }

    @Override
    public Vehicle addVehicle(Vehicle vehicle) {
        return vehicleRepository.save(vehicle);
    }

    @Override
    public Vehicle updateVehicleById(Vehicle vehicle) {
        return vehicleRepository.save(vehicle);
    }

    @Override
    public void deleteVehicleById(Long id) {
        vehicleRepository.deleteById(id);
    }
}
