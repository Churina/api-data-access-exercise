package com.groupwork.services;

import com.groupwork.entities.Review;
import com.groupwork.entities.Vehicle;
import com.groupwork.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;


    // find all reviews
    @Override
    public List<Review> getAllReviews() {

        List<Review> reviews = new ArrayList<>();
        reviews = reviewRepository.findAll();
        return reviews;
    }


    @Override
    public Optional<Review> getReviewById(Long id) {
        return reviewRepository.findById(id);
    }

    @Override
    public List<Review> getReviewsForVehicle(Vehicle vehicle) {

        List<Review> reviews = new ArrayList<>();

        reviews = reviewRepository.findByVehicle(vehicle);
        return reviews;
    }

    @Override
    public Integer getAmountOfReviewForVehicle(Vehicle vehicle) {

       Integer reviews = reviewRepository.countByVehicle(vehicle);
        return reviews;
    }


    @Override
    public Review addReview(Review review) {
        return reviewRepository.save(review);
    }


    @Override
    public Review updateReviewById(Review review) {
        return reviewRepository.save(review);
    }

    @Override
    public void deleteReviewById(Long id) {
        reviewRepository.deleteById(id);
    }

    @Override
    public void deleteReviewByUsername(String username) {

        reviewRepository.deleteByUsername(username);
    }
}
