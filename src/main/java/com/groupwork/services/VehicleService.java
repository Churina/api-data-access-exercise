package com.groupwork.services;

import com.groupwork.entities.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleService {
    List<Vehicle> getVehicles();

    Optional<Vehicle> getById(Long id);

    List<Vehicle> getByYear(Integer year);

    Vehicle getByMakeAndModel(String make, String model);

    Vehicle addVehicle(Vehicle vehicle);
    Vehicle updateVehicleById(Vehicle vehicle);
    void deleteVehicleById(Long id);
}
