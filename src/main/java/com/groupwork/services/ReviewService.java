package com.groupwork.services;

import com.groupwork.entities.Review;
import com.groupwork.entities.Vehicle;

import java.util.List;
import java.util.Optional;

public interface ReviewService {

    List<Review> getAllReviews();

    Optional<Review> getReviewById(Long id);

    List<Review> getReviewsForVehicle(Vehicle vehicle);

    Integer getAmountOfReviewForVehicle(Vehicle vehicle);

    Review addReview(Review review);

    Review updateReviewById(Review review);

    void deleteReviewById(Long id);

    void deleteReviewByUsername(String username);

}
