package com.groupwork.data;

import com.groupwork.entities.Review;
import com.groupwork.entities.Vehicle;
import com.groupwork.repository.ReviewRepository;
import com.groupwork.repository.VehicleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(DataLoader.class);


    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private ReviewRepository reviewRepository;

    private Vehicle vehicle1;
    private Vehicle vehicle2;
    private Vehicle vehicle3;
    private Vehicle vehicle4;
    private Vehicle vehicle5;
    private Vehicle vehicle6;
    private Vehicle vehicle7;

    private Review review1;
    private Review review2;
    private Review review3;
    private Review review4;
    private Review review5;


    @Override
    public void run(String... strings) throws Exception {
        logger.info("Loading data...");

        loadVehicles();
        loadReviews();

    }

    private void loadVehicles(){
        vehicle1 = vehicleRepository.save(new Vehicle("SUV","BMW", "X5",2022));
        vehicle2 = vehicleRepository.save(new Vehicle("Sport","Honda","Accord",2021));
        vehicle3 = vehicleRepository.save(new Vehicle("Hybrid","Toyota","Prius",2020));
        vehicle4 = vehicleRepository.save(new Vehicle("Sedan", "Toyota", "Avalon", 2008));
        vehicle5 = vehicleRepository.save(new Vehicle("Truck", "Ford", "F150", 2008));
        vehicle6 = vehicleRepository.save(new Vehicle("Ven", "Mercedes", "Sprinter", 2010));
        vehicle7 = vehicleRepository.save(new Vehicle("Sedan", "Toyota", "Corolla", 2002));
    }
    private void loadReviews() {
        review1 = reviewRepository.save(new Review("Great", "Good Car,Great breaks, wheels are nice", 4,2022-06-23,"user1",vehicle1));
        review2 = reviewRepository.save(new Review("Bad", "Ugly, expensive car", 1, 2020-11-03,"user2",vehicle2));
        review3 = reviewRepository.save(new Review("I like it", "Good for woman",4,2019-03-12,"user3",vehicle3));
        review4 = reviewRepository.save(new Review("nice car","quite enjoyable",3,2020-05-07,"user4",vehicle3));
        review5 = reviewRepository.save(new Review("plain boxy car", "quite reliable", 4, 2020-02-07, "user4", vehicle6 ));
    }

}
