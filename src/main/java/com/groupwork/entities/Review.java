package com.groupwork.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

@Entity
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)

    private Long id;
    private String title;
    private String description;
    private Integer rating;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Integer date;
    private String username;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @NotNull(message = "Vehicle is Required")
    private Vehicle vehicle;


    public Review() {
    }

    public Review(String title, String description, Integer rating, Integer date, String username, Vehicle vehicle) {
        this.title = title;
        this.description = description;
        this.rating = rating;
        this.date = date;
        this.username = username;
        this.vehicle = vehicle;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {return title;}

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getDate() { return date; }

    public void setDate(Integer date) { this.date = date; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) { this.vehicle = vehicle; }
}
