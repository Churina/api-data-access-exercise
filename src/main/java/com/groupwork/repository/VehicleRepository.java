package com.groupwork.repository;

import com.groupwork.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle,Long> {
    List<Vehicle> findByYear(Integer year);
    Vehicle findByMakeAndModel(String make,String model);
}
