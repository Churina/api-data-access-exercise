package com.groupwork.repository;

import com.groupwork.entities.Review;
import com.groupwork.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
    List<Review> findByVehicle(Vehicle vehicle);

    Integer countByVehicle(Vehicle vehicle);

    @Transactional
    void deleteByUsername(String username);
}
