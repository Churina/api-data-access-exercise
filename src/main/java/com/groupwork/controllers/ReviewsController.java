package com.groupwork.controllers;

import com.groupwork.entities.Review;
import com.groupwork.entities.Vehicle;
import com.groupwork.services.ReviewService;
import com.groupwork.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/reviews")
public class ReviewsController {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private VehicleService vehicleService;

    @GetMapping
    public List<Review>  getAllReviews() {

        List<Review> reviews = reviewService.getAllReviews();

        return reviews;
    }


    @GetMapping(value = "/{id}")
    public Optional<Review> getReviewById(@PathVariable Long id){
        return reviewService.getReviewById(id);
    }

    @GetMapping(value = "/vehicles")
    public List <Review> getReviewsForVehicleMakeAndModel(@RequestParam String make, @RequestParam String model){

        Vehicle vehicle = vehicleService.getByMakeAndModel(make,model);

        return reviewService.getReviewsForVehicle(vehicle);
    }

    @GetMapping(value = "/vehicles/count")
    public Integer getAmountOfReviewsForVehicleMakeAndModel(@RequestParam String make, @RequestParam String model){

        Vehicle vehicle = vehicleService.getByMakeAndModel(make,model);

        return reviewService.getAmountOfReviewForVehicle(vehicle);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Review saveNewReview(@RequestBody Review review){
        return reviewService.addReview(review);
    }


    @PutMapping(value = "/{id}")
    public Review updateReviewById(@RequestBody Review review){
        return reviewService.updateReviewById(review);
    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteReviewById(@PathVariable Long id){

        reviewService.deleteReviewById(id);
    }


    //@RequestMapping(value = "/delete/{username}", method = RequestMethod.DELETE)
    @DeleteMapping(value = "/delete/{username}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteReviewByUsername(@PathVariable String username){

        reviewService.deleteReviewByUsername(username);
    }
}




