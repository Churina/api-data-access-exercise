package com.groupwork.controllers;

import com.groupwork.entities.Vehicle;
import com.groupwork.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vehicles")
public class VehiclesController {
    @Autowired
    private VehicleService vehicleService;


    @GetMapping
    public List<Vehicle> getAllVehicles(){

        List<Vehicle> vehicles = vehicleService.getVehicles();
         return vehicles;
    }


    @GetMapping(value = "/{id}")
    public Optional<Vehicle> getById(@PathVariable Long id) {

        return vehicleService.getById(id);
    }


    @GetMapping(value = "/")
    public List<Vehicle> getByYear(@RequestParam Integer year) {

        return vehicleService.getByYear(year);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Vehicle saveNewVehicle(@RequestBody Vehicle vehicle){
        return vehicleService.addVehicle(vehicle);
    }

    @PutMapping(value = "/{id}")
    public Vehicle updateVehicleById(@RequestBody Vehicle vehicle){
        return vehicleService.updateVehicleById(vehicle);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteVehicleById(@PathVariable Long id){

        vehicleService.deleteVehicleById(id);
    }
}
