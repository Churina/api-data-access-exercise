# API Data Access Exercise


## Description
A used vehicle resale company wants to create a website to record car reviews of
different makes and models to help their customers pick the right car for them. They
have a team working on the frontend, but want you to create an api to access the
database.


## Contributing, Authors and acknowledgment
Churina Sa
Jazmarie Moncrieft


## Database Information

database name: POSTGRESQL
username: postgres
password: root
Port:5432

## Postman collection URL
https://api.postman.com/collections/25365335-e71236e1-8d0f-4a89-8b1a-93aafdf95956?access_key=PMAT-01GRANQJBFGEN7BGKVF0G89JCK
